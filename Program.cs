﻿using System;
using System.Configuration;

namespace CfgTest
{
	class Program
	{
		static void Main(string[] args)
		{
			string key1Value = ConfigurationManager.AppSettings["key1"];
			string key2Value = ConfigurationManager.AppSettings["key2"];

			Console.WriteLine($"{key1Value}, {key2Value}");

			Console.ReadLine();
		}
	}
}
